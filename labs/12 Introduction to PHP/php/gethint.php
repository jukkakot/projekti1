<?php
// Array with names
$all_arrays = array(
    array("firstname" => "Anna"),
    array("firstname" => "Arja"),
    array("firstname" => "Leo"),
    array("firstname" => "Aleksi"),
    array("firstname" => "Aino"),
    array("firstname" => "Sari"),
    array("firstname" => "Teemu"),
    array("firstname" => "Heikki"),
    array("firstname" => "Inga"),
    array("firstname" => "Maltti"),
    array("firstname" => "Kirsi"),
    array("firstname" => "Laila"),
    array("firstname" => "Nina"),
    array("firstname" => "Orvokki"),
    array("firstname" => "Pekka"),
    array("firstname" => "Amanda"),
    array("firstname" => "Raimo"),
    array("firstname" => "Sirpa"),
    array("firstname" => "Liisa"),
    array("firstname" => "Erkki"),
    array("firstname" => "Aimo"),
    array("firstname" => "Risto"),
    array("firstname" => "Markku"),
    array("firstname" => "Peppi"),
    array("firstname" => "Visa"),
    array("firstname" => "Elisa"),
    array("firstname" => "Eila"),
    array("firstname" => "Ellen"),
    array("firstname" => "Valtti"),
    array("firstname" => "Vihtori")
);
// get the q parameter from URL
$q = $_REQUEST["q"];

$hint = "";
$all_hints = array();
// lookup all hints from array if $q is different from ""
if ($q !== "") {
    $q = strtolower($q);
    $len=strlen($q);
    foreach($all_arrays as $name) {
        if (stristr($q, substr($name["firstname"], 0, $len))) {
            if ($hint === "") {
                $hint = $name["firstname"];
                array_push($all_hints,array($name));
            } else {
                $hint .= $name["firstname"];
                array_push($all_hints,array($name));
            }
        }
    }
}

// Output "no suggestion" if no hint was found or output correct values
//echo $hint === "" ? "no suggestion" : $hint;
echo json_encode($all_hints);
?>
