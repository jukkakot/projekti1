<?php

class Dice {
    private  $faces;
    private  $bias;
    private  $freqs = array();
    
    // Constructor
    public function __construct($faces, $bias) {
        $this->faces = $faces;
        $this->bias = $bias;
        echo "<h1>You made normal dice</h1>";
    }
    
    public function cast() {
        if ($this->bias > 0) {
            if (rand(1, 10) <= $this->bias * 10) {
                $this->freqs[$this->faces]++;
                return $this->faces;
            }
            $res = rand(1, $this->faces - 1);
            $this->freqs[$res]++;
            return $res;
        } else {
            $res = rand(1, $this->faces);
            $this->freqs[$res]++;
            return $res;
        }
    }

    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    public function getAverage() {
        $result = 0;
        $divider=0;
        for ($i = 1;$i <=$this->faces;$i++){
            $result += $i* $this->freqs[$i];
            $divider += $this->freqs[$i];
        }
        return $result/$divider;
    }
    }
    class PhysicalDice extends Dice {
        private $material;
        function __construct($faces, $bias,$material ) {
            parent::__construct($faces, $bias);
            $this->material = $material;
            echo "<h1>You made PhysicalDice! made out of $this->material</h1>";
        }
    }
?>