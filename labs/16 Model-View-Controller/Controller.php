<?php
class Controller {
    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function list_it() {
        $this->messages = $this->model->messages();
        include("View.php");
    }
    public function send() {
        if($_POST["name"] != "") {
            setcookie("name", $_POST["name"]);
        }
        $this->model->add_message($_POST["message"]." -".$_COOKIE["name"]);
        header("Location: Chat.php?action=list_it");
    }
}
?>