
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;
var h1 = document.createElement('h1');
h1.id = 'h1';
document.body.appendChild(h1);
    var table = document.createElement('table');
    var trHeading = document.createElement('tr');
    var title = document.createElement('th');
    title.innerHTML = "Title";
    var year = document.createElement('th');
    year.innerHTML = "Year"
    var isbn = document.createElement('th');
    isbn.innerHTML = "Isbn"
    var authors = document.createElement('th');
    authors.innerHTML = "Author"
    trHeading.appendChild(title);
    trHeading.appendChild(year);
    trHeading.appendChild(isbn);
    trHeading.appendChild(authors);
    table.appendChild(trHeading);
    for (var i=0; i < books.length; i++) {
        var tr = document.createElement('tr');
        tr.id = i;
        var title = document.createElement('th');
        title.innerHTML = books[i].title;
        var year = document.createElement('th');
        year.innerHTML = books[i].year;
        var isbn = document.createElement('th');
        isbn.innerHTML = books[i].isbn;
        var authors = document.createElement('th');
        authors.innerHTML = books[i].authors;

        tr.appendChild(title);
        tr.appendChild(year);
        tr.appendChild(isbn);
        tr.appendChild(authors);

        table.appendChild(tr);
    }
    table.setAttribute('border','1');
    document.body.appendChild(table);
    document.getElementById('0').onclick = showTitle;
    document.getElementById('1').onclick = showTitle;
    document.getElementById('2').onclick = showTitle;
    document.getElementById('3').onclick = showTitle;
    document.getElementById('4').onclick = showTitle;
function showTitle() {
    var text = document.getElementById('h1');
    text.innerHTML = books[this.id].title;

}

